package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
)

func Home(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		r.ParseForm()
		log.Printf("Handling new vote from %s: %s [%s]\n", r.Header.Get("X-Forwarded-For"), r.URL.Path, r.UserAgent())
		for k, v := range r.PostForm {
			log.Println(k + ":", v)
		}

		http.ServeFile(w, r, "thanks.html")
	} else {
		log.Printf("Serve home page to %s: %s [%s]\n", r.Header.Get("X-Forwarded-For"), r.URL.Path, r.UserAgent())
		http.ServeFile(w, r, "home.html")
	}
}

func main() {
	var bind = flag.String("bind", "0.0.0.0:8081", "Bind port/socket")
	flag.Parse()

	log.Println("Registering handlers...")
	http.HandleFunc("/", Home)
	log.Println(fmt.Sprintf("Ready, listening on %s", *bind))
	if err := http.ListenAndServe(*bind, nil); err != nil {
		log.Fatal("Unable to listen and serve: ", err)
	}
}
